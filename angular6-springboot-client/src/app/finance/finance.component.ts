import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee-service';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/internal/Observable';
import { Router } from '@angular/router';

@Component({
  selector: 'app-finance',
  templateUrl: './finance.component.html',
  styleUrls: ['./finance.component.css']
})
export class FinanceComponent implements OnInit {
  loginForm: FormGroup;
  submitted: boolean = false;
  employeeslist: Observable<any>[]=[];
  empobj:Observable<any>[]=[];
  delRec:String;
  count : any=0;

  constructor( private employeeService: EmployeeService,private router: Router) {
  
  }
  ngOnInit() {
     this.reloadData();
  }  
  reloadData() {    
  this.employeeService.getAllEmployee().subscribe(data=>{
    this.employeeslist=data;
    console.log(this.employeeslist);  // don't put ouside subscribe
  }); 
  }  
 
  public  transferTDS()
  {    
    this.employeeService.transferTDS().subscribe(data=>{
      this.count=data;
      console.log(this.count+ "records updated");
  });
}
}