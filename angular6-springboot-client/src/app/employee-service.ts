import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IEmployee } from 'src/model/IEmp';
import { FormsModule } from '@angular/forms';
import { IEmpTds } from 'src/model/IEmpTds';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};
@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private baseUrl = 'http://localhost:8080/api/v1/';


  constructor(private http: HttpClient) { }

  createEmployee(employee: Object): Observable<Object> {
    return this.http.post(this.baseUrl + 'employees', employee);
  }
  // updateEmployee(id: number, value: any): Observable<Object> {
  //   return this.http.put(`${this.baseUrl}/${id}`, value);
  // }
  deleteEmployee(id: number): Observable<any> {
    return this.http.delete(this.baseUrl + 'employees/' + id, { responseType: 'text' });
  }
  getAllEmployee() {

    console.log(this.http.get<IEmployee[]>(this.baseUrl + 'employees'));
    // myList Number[]=this.http.get<Number[]>(this.baseUrl+'employees')
    return this.http.get<any[]>(this.baseUrl + 'employees');

  }

  // Get User By Id
  getEmpById(id: number): Observable<any> {

    console.log(this.http.get<IEmployee[]>(this.baseUrl + 'employees/' + id));
    return this.http.get<any[]>(this.baseUrl + 'employees/' + id);

  }
  public transferTDS() {
    console.log(this.http.get<any[]>(this.baseUrl + 'finance'));
    return this.http.get<any[]>(this.baseUrl + 'finance');
  }

}