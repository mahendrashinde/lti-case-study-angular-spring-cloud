import { Observable } from "rxjs";
import { EmployeeService } from "./../employee-service";
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from '@angular/router';

@Component({
  selector: "app-employee-list",
  templateUrl: "./employee-list.component.html",
  styleUrls: ["./employee-list.component.css"]
})
export class EmployeeListComponent implements OnInit {
  loginForm: FormGroup;
  submitted: boolean = false;
  employeeslist: Observable<any>[] = [];
  empobj: Observable<any>[] = [];
  delRec: String;
  count: any = 0;

  constructor(private employeeService: EmployeeService, private router: Router) {
  }
  ngOnInit() {
    this.reloadData();
  }
  reloadData() {
    this.employeeService.getAllEmployee().subscribe(data => {
      this.employeeslist = data;
      console.log(this.employeeslist);  // don't put ouside subscribe
    });
  }

  public getEmpById(e) {

    this.employeeService.getEmpById(e).subscribe(data => {
      this.empobj = data;
      console.log(this.empobj);
    });

  }
  public transferTDS() {
    this.employeeService.transferTDS().subscribe(data => {
      this.count = data;
      console.log(this.count + "records updated");
    });
  }

  deleteEmployee(id: number) {
    this.employeeService.deleteEmployee(id)
      .subscribe(
        data => {
          this.delRec = data;
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }
}

