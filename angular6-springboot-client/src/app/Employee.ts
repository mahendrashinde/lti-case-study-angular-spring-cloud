export class Employee {
    id: number;
    firstName: string;
    lastName: string;
    emailId: string;
    annPack: number;
    panNo: number;
}