export interface IEmpTds {
    tranId: number;
    empName: string;
    panNo:number;
    month: number;
    year:number;
    tdsAmount: number;
    emailId:String; 
    }
   