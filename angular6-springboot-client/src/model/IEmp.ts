export interface IEmployee {
    id: number;
    firstName: string;
    lastName: string;
    emailId: string;
    annPack:number;
    panNo:number;
    }