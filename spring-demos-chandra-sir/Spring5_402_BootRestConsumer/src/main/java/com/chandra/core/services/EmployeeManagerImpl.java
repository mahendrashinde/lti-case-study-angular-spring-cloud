package com.chandra.core.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.chandra.core.dtos.Employee;
import com.chandra.core.exceptions.HrException;

// This annotation detects the bean as of type @Component.  The bean detection is possible because of @ComponentScan
@Service("empManager")
public class EmployeeManagerImpl implements EmployeeManager {
	@Autowired
	private RestTemplate template;
	
	@Autowired // To connect to driver of Eureka Server.
	private DiscoveryClient discoveryClient;
	
	public EmployeeManagerImpl() {
		System.out.println("The EmployeeManagerImpl bean is created.");
	}
	
	@Override
	//@Transactional(readOnly=true)
	public Employee getEmpDetails(int empId) throws HrException {
		System.out.println("Control in getEmpList() of EmployeeManager");
		
		List<ServiceInstance> instances=discoveryClient.getInstances("SPRING5_401_BOOTRESTPRODUCER");
		ServiceInstance serviceInstance=instances.get(0);
		
		String baseUrl=serviceInstance.getUri().toString();
		
		System.out.println("Base URL: " + baseUrl);
		String actualUrl = baseUrl+"/emps/empDetails/"+empId;
		
		Employee empDetails = null;
		try{
				empDetails = template.getForObject(actualUrl, Employee.class);
				System.out.println("Details from Rest Template\n" + empDetails);
		}catch (Exception ex)
		{
			System.out.println(ex);
		}
		
        return empDetails;
	}
}
