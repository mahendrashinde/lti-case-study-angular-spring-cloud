package com.chandra.core.dtos;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/*
 * JSON
 	{
 		"id":300,
 		"firstName":"Chandra",
 		"lastName":"Desh",
 		"email":"abc@gmail.com",
 		"jobId":"IT_PROG",
 		"salary":6000,
 		"commission":null,
 		"hireDate":null
 	}
 		
 */
@Entity(name="Emp")
@Table(name="EMPLOYEES")
public class Employee {
	@Id
	@Column(name="EMPLOYEE_ID")
	private int id;
	
	@Column(name="FIRST_NAME")
	private String firstName;
	
	@Column(name="LAST_NAME")
	private String lastName;
	
	@Column(name="EMAIL")
	private String email;
	
	@Column(name="HIRE_DATE")
	//@Transient
	private Date hireDate;
	
	@Column(name="JOB_ID")
	private String jobId;
	
	@Column(name="SALARY")
	private Float salary;
	
	@Column(name="COMMISSION_PCT")
	private Float commission;

	public Employee(int id, String lastName) {
		super();
		this.id = id;
		this.lastName = lastName;
	}

	public Employee() {
		super();
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getHireDate() {
		return hireDate;
	}

	public void setHireDate(Date hireDate) {
		this.hireDate = hireDate;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public Float getSalary() {
		return salary;
	}

	public void setSalary(Float salary) {
		this.salary = salary;
	}

	public Float getCommission() {
		return commission;
	}

	public void setCommission(Float commission) {
		this.commission = commission;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email
				+ ", hireDate=" + hireDate + ", jobId=" + jobId + ", salary=" + salary + ", commission=" + commission
				+ "]";
	}
}
