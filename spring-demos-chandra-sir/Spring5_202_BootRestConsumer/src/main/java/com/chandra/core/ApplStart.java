package com.chandra.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@ComponentScan("com.chandra.web.controllers, com.chandra.core.services")
@EntityScan("com.chandra.core.dtos")
@EnableDiscoveryClient
public class ApplStart {
	public static void main(String[] args) {
		SpringApplication.run(ApplStart.class, args);
	}
	
	@Bean
	public RestTemplate getRestTemplate(RestTemplateBuilder builder) {
		RestTemplate template = builder.build();
		
		System.out.println("RestTemplate Created.");
		return template;
	}
}
