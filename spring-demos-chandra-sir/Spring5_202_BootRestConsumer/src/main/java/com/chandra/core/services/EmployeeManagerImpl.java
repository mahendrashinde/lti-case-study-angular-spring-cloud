package com.chandra.core.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.chandra.core.dtos.Employee;
import com.chandra.core.exceptions.HrException;

// This annotation detects the bean as of type @Component.  The bean detection is possible because of @ComponentScan
@Service("empManager")
public class EmployeeManagerImpl implements EmployeeManager {
	@Autowired
	private RestTemplate template;
	
	@Autowired // To connect to driver of Eureka Server.
	private DiscoveryClient discoveryClient;
	
	public EmployeeManagerImpl() {
		System.out.println("The EmployeeManagerImpl bean is created.");
	}
	
	@Override
	//@Transactional(readOnly=true)
	public List<Employee> getEmpList() throws HrException {
		System.out.println("Control in getEmpList() of EmployeeManager");
		
		List<ServiceInstance> instances=discoveryClient.getInstances("SPRING5_201_BOOTRESTPRODUCER");
		ServiceInstance serviceInstance=instances.get(0);
		
		String baseUrl=serviceInstance.getUri().toString();
		
		System.out.println("Base URL: " + baseUrl);
		String actualUrl=baseUrl+"/emps/empList";
		
		List<Employee> empList = null;
		//ResponseEntity<String> response=null;
		try{
		/*response=restTemplate.exchange(baseUrl,
				HttpMethod.GET, getHeaders(),String.class);*/
		
		empList = template.getForObject(actualUrl, List.class);
		System.out.println("List from Rest Template\n" + empList);
		}catch (Exception ex)
		{
			System.out.println(ex);
		}
		//System.out.println(response.getBody());
        return empList;
	}

	/*@Override
	public Employee getEmpDetails(int id) throws HrException {
		String url = locator.findService("getEmpDetails") + id;
		Employee emp = template.getForObject(url, Employee.class);
		return emp;
	}

	@Override
	@Transactional
	public Employee insertNewEmp(Employee emp) throws HrException {
		//return daos.insertNewEmp(emp);
		return null;
	}*/
}
