package com.chandra.web.controllers;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.chandra.core.dtos.Employee;
import com.chandra.core.exceptions.HrException;
import com.chandra.core.services.EmployeeManager;

@RestController
@RequestMapping("/emps")
@CrossOrigin(origins="*",allowedHeaders="*")
public class EmpController {
	
	@Autowired
	private EmployeeManager manager;
	
	public EmpController() {
		System.out.println("The Controller is created.");
	}
	
	// http://localhost:8092/emps/empList
	@GetMapping(value="/empList",produces="application/json")
    public List<Employee> getEmpList() throws HrException {
		List<Employee> empList = manager.getEmpList();
		/*List<ServiceInstance> instances=discoveryClient.getInstances("SPRING5_201_BOOTRESTPRODUCER");
		ServiceInstance serviceInstance=instances.get(0);
		
		String baseUrl=serviceInstance.getUri().toString();
		
		System.out.println("Base URL: " + baseUrl);
		baseUrl=baseUrl+"/emps/empList";
		
		RestTemplate restTemplate = new RestTemplate();
		List<Employee> empList = null;
		//ResponseEntity<String> response=null;
		try{
		response=restTemplate.exchange(baseUrl,
				HttpMethod.GET, getHeaders(),String.class);
		
		empList = restTemplate.getForObject(baseUrl, List.class);
		System.out.println("List from Rest Template\n" + empList);
		}catch (Exception ex)
		{
			System.out.println(ex);
		}
		//System.out.println(response.getBody());*/
		return empList;
    }
	
	/*private static HttpEntity<?> getHeaders() throws IOException {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		return new HttpEntity<>(headers);
	}*/
}
