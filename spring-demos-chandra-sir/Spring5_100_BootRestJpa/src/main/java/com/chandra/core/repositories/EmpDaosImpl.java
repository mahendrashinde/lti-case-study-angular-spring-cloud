package com.chandra.core.repositories;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.chandra.core.dtos.Employee;
import com.chandra.core.exceptions.HrException;

@Repository("empDao")
public class EmpDaosImpl implements EmpDaos {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public List<Employee> getEmpList() throws HrException {
		String sql = "from Emp";
		Query qry = entityManager.createQuery(sql);
		//qry.setMaxResults(10);
		//qry.setFirstResult(11);
		
		List<Employee> empList = qry.getResultList();
		return empList;
	}
	
	@Override
	public Employee getEmpDetails(int id) throws HrException {
		Employee emp = entityManager.find(Employee.class, id);
		return emp;
	}
	
	@Override
	public Employee insertNewEmp(Employee emp) throws HrException {
		entityManager.persist(emp);
		
		return emp;
	}
	
	/*@Autowired
	private SessionFactory sessionFactory;
	
	@Override
   public List<Employee> getEmpList() {
      @SuppressWarnings("unchecked")
      TypedQuery<Employee> query = sessionFactory.openSession().createQuery("from Emp");
      return query.getResultList();
   }

	@Override
	public Employee getEmpDetails(int id) throws HrException {
		Session session = sessionFactory.openSession();
		Employee emp = session.find(Employee.class, id);
		return emp;
	}

	@Override
	public Employee insertNewEmp(Employee emp) throws HrException {
		Session session = sessionFactory.openSession();
		Transaction tr = session.beginTransaction();
		try {
			session.persist(emp);
			tr.commit();
		} catch (Exception e) {
			tr.rollback();
		}
		
		return emp;
	}*/
	
	
}
