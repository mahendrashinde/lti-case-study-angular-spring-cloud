package com.chandra.core.repositories;

import java.util.List;

import com.chandra.core.dtos.Employee;
import com.chandra.core.exceptions.HrException;

public interface EmpDaos {

	public List<Employee> getEmpList() throws HrException;
	public Employee getEmpDetails(int id) throws HrException;
	public Employee insertNewEmp(Employee emp) throws HrException;
}
