package com.chandra.core.services;

import java.util.HashMap;

import org.springframework.stereotype.Service;

@Service
public class ServiceLocator {
	private HashMap<String, String> map;
	private String baseUrl = "http://localhost:8082/emps/";
	
	public ServiceLocator() {
		map = new HashMap<>();
		
		map.put("listAllEmps", baseUrl + "empList");
		map.put("getEmpDetails", baseUrl + "empDetails/");
	}
	
	public String findService(String serviceName) {
		return map.get(serviceName);
	}
}
