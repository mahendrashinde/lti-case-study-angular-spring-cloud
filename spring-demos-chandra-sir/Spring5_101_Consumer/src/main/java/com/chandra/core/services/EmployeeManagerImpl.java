package com.chandra.core.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.chandra.core.dtos.Employee;
import com.chandra.core.exceptions.HrException;

// This annotation detects the bean as of type @Component.  The bean detection is possible because of @ComponentScan
@Service("empManager")
public class EmployeeManagerImpl implements EmployeeManager {
	@Autowired
	private RestTemplate template;
	
	@Autowired
	private ServiceLocator locator;
	
	public EmployeeManagerImpl() {
		System.out.println("The EmployeeManagerImpl bean is created.");
	}
	
	@Override
	//@Transactional(readOnly=true)
	public List<Employee> getEmpList() throws HrException {
		System.out.println("Control in getEmpList() of EmployeeManager");
		String url = locator.findService("listAllEmps");
		//String url = "http://localhost:8082/emps/empList";
		List<Employee> empList = template.getForObject(url, List.class);
		return empList;
	}

	@Override
	public Employee getEmpDetails(int id) throws HrException {
		String url = locator.findService("getEmpDetails") + id;
		//String url = "http://localhost:8082/emps/empDetails/"+id;
		Employee emp = template.getForObject(url, Employee.class);
		return emp;
	}

	@Override
	@Transactional
	public Employee insertNewEmp(Employee emp) throws HrException {
		//return daos.insertNewEmp(emp);
		return null;
	}
}
