package com.chandra.web.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.chandra.core.dtos.Employee;
import com.chandra.core.exceptions.HrException;
import com.chandra.core.services.EmployeeManager;

@RestController
@RequestMapping("/emps")
@CrossOrigin(origins="*",allowedHeaders="*")
public class EmpController {
	@Autowired
	@Qualifier("empManager")
	private EmployeeManager manager;

	public EmpController() {
		System.out.println("The Controller is created.");
	}
	
	// http://localhost:8083/emps/empList
	@GetMapping(value="/empList",produces="application/json")
    public List<Employee> getEmpList() throws HrException {
		List<Employee> empList = manager.getEmpList();
		System.out.println(empList);
        return empList;
    }
	
	// http://localhost:8083/emps/empDetails/104
	@GetMapping(value="/empDetails/{id}",produces="application/json")
    public Employee getEmpDetails(@PathVariable("id") int id) throws HrException {
		Employee emp = manager.getEmpDetails(id);
		
        return emp;
    }
	
	// http://localhost:8082/emps/newEmp
	/*@PostMapping(value="/newEmp", consumes="application/json")
	public @ResponseBody Employee insertNew(@RequestBody Employee emp) throws HrException {
		System.out.println(emp);
		
		return empManager.insertNewEmp(emp);
	}*/
}
