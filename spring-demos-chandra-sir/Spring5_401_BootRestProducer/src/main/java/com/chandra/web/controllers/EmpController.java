package com.chandra.web.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.chandra.core.dtos.Employee;
import com.chandra.core.exceptions.HrException;
import com.chandra.core.services.EmployeeManager;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@RestController
@RequestMapping("/emps")
@CrossOrigin(origins="*",allowedHeaders="*")
public class EmpController {
	@Autowired // By default it is autowiring by type.
	@Qualifier("empManager") 
	private EmployeeManager empManager;

	public EmpController() {
		System.out.println("The Controller is created.");
	}
	
	// http://localhost:8091/emps/empList
	@GetMapping(value="/empList",produces="application/json")
    public List<Employee> getEmpList() throws HrException {
		List<Employee> empList = empManager.getEmpList();
		System.out.println(empList);
        return empList;
    }
	
	// http://localhost:8091/emps/empDetails/104
	// http://localhost:8091/emps/empDetails/105
	@HystrixCommand(fallbackMethod = "getDataFallBack")
	@GetMapping(value="/empDetails/{id}",produces="application/json")
    public Employee getEmpDetails(@PathVariable("id") int id) throws HrException {
		Employee emp = empManager.getEmpDetails(id);
		
		if (emp.getId()==105) {
			throw new RuntimeException("Invoke fallback mechanism.");
		}
        return emp;
    }
	
	public Employee getDataFallBack(int id) {
		Employee emp = new Employee();
		emp.setId(id);
		emp.setFirstName("fallback-emp1");
		emp.setEmail("fallback-email");
		emp.setLastName("fallback-lastname");

		return emp;
	}
}
