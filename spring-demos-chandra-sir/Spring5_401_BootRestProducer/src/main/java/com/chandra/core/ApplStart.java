package com.chandra.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.chandra.core.repositories, com.chandra.core.services, com.chandra.web.controllers")
@EntityScan("com.chandra.core.dtos")
@EnableDiscoveryClient
@EnableCircuitBreaker
public class ApplStart {
	public static void main(String[] args) {
		SpringApplication.run(ApplStart.class, args);
	}
}
