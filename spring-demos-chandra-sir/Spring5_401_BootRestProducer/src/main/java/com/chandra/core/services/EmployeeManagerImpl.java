package com.chandra.core.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.chandra.core.dtos.Employee;
import com.chandra.core.exceptions.HrException;
import com.chandra.core.repositories.EmpDaos;

@Service("empManager")
public class EmployeeManagerImpl implements EmployeeManager {
	@Autowired
	public EmpDaos daos;
	
	public EmployeeManagerImpl() {
		System.out.println("The EmployeeManagerImpl bean is created.");
	}
	
	@Override
	//@Transactional(readOnly=true)
	public List<Employee> getEmpList() throws HrException {
		System.out.println("Control in getEmpList() of EmployeeManager");
		return daos.getEmpList();
	}
	
	@Override
	public String toString() {
		return "EmployeeManagerImpl.toString()";
	}

	@Override
	public Employee getEmpDetails(int id) throws HrException {
		
		return daos.getEmpDetails(id);
	}

	@Override
	@Transactional
	public Employee insertNewEmp(Employee emp) throws HrException {
		return daos.insertNewEmp(emp);
	}
}
