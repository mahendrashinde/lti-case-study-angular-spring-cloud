package com.chandra.web.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.chandra.core.dtos.Employee;
import com.chandra.core.exceptions.HrException;
import com.chandra.core.services.EmployeeManager;

@RestController
@RequestMapping("/emps")
@CrossOrigin(origins="*",allowedHeaders="*")
public class EmpController {
	
	@Autowired
	private EmployeeManager manager;
	
	public EmpController() {
		System.out.println("The Controller is created.");
	}
	
	// http://localhost:8093/emps/empList
	@GetMapping(value="/empList",produces="application/json")
    public List<Employee> getEmpList() throws HrException {
		List<Employee> empList = manager.getEmpList();
		
		return empList;
    }
}
