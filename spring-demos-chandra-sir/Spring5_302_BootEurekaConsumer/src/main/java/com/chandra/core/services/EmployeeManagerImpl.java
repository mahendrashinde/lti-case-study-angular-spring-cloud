package com.chandra.core.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.chandra.core.dtos.Employee;
import com.chandra.core.exceptions.HrException;

// This annotation detects the bean as of type @Component.  The bean detection is possible because of @ComponentScan
@Service("empManager")
@RibbonClient(name="Spring5_301-2_BootRestProducer")
public class EmployeeManagerImpl implements EmployeeManager {
	@Autowired
	//@LoadBalanced
	private RestTemplate template;
	
	@Autowired // To connect to driver of Eureka Server.
	private LoadBalancerClient loadBalancer;
	
	public EmployeeManagerImpl() {
		System.out.println("The EmployeeManagerImpl bean is created.");
	}
	
	@Override
	//@Transactional(readOnly=true)
	public List<Employee> getEmpList() throws HrException {
		System.out.println("Control in getEmpList() of EmployeeManager");
		
		ServiceInstance instance=loadBalancer.choose("SPRING5_301-2_BOOTRESTPRODUCER");
		
		String baseUrl = instance.getUri().toString();
		
		System.out.println("Base URL: " + baseUrl);
		String actualUrl = baseUrl+"/emps/empList";
		
		
		List<Employee> empList = null;
		try{
				empList = template.getForObject(actualUrl, List.class);
				System.out.println("List from Rest Template\n" + empList.get(0));
		}catch (Exception ex)
		{
			System.out.println(ex);
		}
		
        return empList.subList(1, 3);
	}
}
